from django.shortcuts import render
from django.views.generic import TemplateView
# Create your views here.

class HomePageView(TemplateView):
    template_name = 'home.html'

class AboutPageView(TemplateView):
    template_name = 'about.html'

class ContactPageView(TemplateView):
    template_name = 'contact.html'

class StartProjectView(TemplateView):
    template_name = 'start_a_project.html'

class StartAnotherProjectView(TemplateView):
    template_name = 'start_another_project.html'

class FeedBackView(TemplateView):
    template_name = 'feedback.html'

class PrivacyPolicyView(TemplateView):
    template_name = 'privacy-policy.html'
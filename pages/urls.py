from django.urls import path
from . import views

urlpatterns = [
    path('', views.HomePageView.as_view(), name='home'),
    path('about/', views.AboutPageView.as_view(), name='about'),
    path('contact/', views.ContactPageView.as_view(), name='contact'),
    path('start_a_project/', views.StartProjectView.as_view(), name='start_a_project'),
    path('start_another_project/', views.StartAnotherProjectView.as_view(), name='start_another_project'),
    path('feedback/', views.FeedBackView.as_view(), name='feedback'),
    path('privacy-policy/', views.PrivacyPolicyView.as_view(), name='privacy-policy'),
]
import django_filters

from . import models

class EquipmentFilter(django_filters.FilterSet):
    class Meta:
        model = models.Equipment
        fields = ['name']


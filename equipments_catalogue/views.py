from django.shortcuts import render
from django.views.generic import ListView, DetailView, TemplateView
from . import models
from django.contrib.auth.mixins import LoginRequiredMixin

from .filters import EquipmentFilter


# Create your views here.

class EquipmentCategoryView(LoginRequiredMixin, ListView):
    model = models.Category
    template_name = 'equipment_categories.html'
    login_url = 'login'


def EquipmentListByCategory(request, cats):
    equipment_category = models.Equipment.objects.filter(category=cats)
    categories = models.Category.objects.all()
    equipments = models.Equipment.objects.all()
    print("************************************")
    print(equipment_category)
    print("*****************************")

    myFilter = EquipmentFilter(request.GET, queryset=equipments)
    equipments = myFilter.qs

    return render(request, 'equipment_by_category.html', {'cats': cats , 'equipment_category': equipment_category, 'categories': categories, 'myFilter': myFilter})

class EquipmentListView(LoginRequiredMixin, ListView):
    model = models.Equipment
    template_name = 'equipment_list.html'

    def get_queryset(self):
        query = self.request.GET.get('q')
        object_list = models.Equipment.objects.filter(name__icontains=query)
        return object_list

class EquipmentDetailView(LoginRequiredMixin, DetailView):
    model = models.Equipment
    template_name = 'equipment_detail.html'

class ListGearView(TemplateView):
    template_name = 'list-gear.html'
from django.apps import AppConfig


class EquipmentsCatalogueConfig(AppConfig):
    name = 'equipments_catalogue'

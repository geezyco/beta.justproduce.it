from django.urls import path
from . import views

app_name = 'equipments_catalogue'

urlpatterns = [
    #path('equipment_list/', views.EquipmentListView.as_view(), name='equipment_list'),
    path('equipment_categories/', views.EquipmentCategoryView.as_view(), name='equipment_categories'),
    path('equipment_by_category/<str:cats>/', views.EquipmentListByCategory, name='equipment_by_category'),
    path('equipment/<slug>/', views.EquipmentDetailView.as_view(), name='equipment_detail'),
    path('list-gear/', views.ListGearView.as_view(), name='list_gear'),
    path('search-results/', views.EquipmentListView.as_view(), name='search-results'),

]
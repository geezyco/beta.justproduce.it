from django.contrib import admin
from . import models

# Register your models here.
@admin.register(models.Equipment)
class EquipmentAdmin(admin.ModelAdmin):
    list_display = ('name', 'vendor_name', 'category', 'price')
    #prepopulated_fields = {'slug': ('name', )}
    search_fields = ['name', 'vendor_name', 'category']
    list_filter = ['category', 'vendor_name', 'price']
admin.site.register(models.Category)
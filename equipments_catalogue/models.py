from django.db import models
from django.shortcuts import reverse
from django.utils.text import slugify

# Create your models here.

CATEGORY_CHOICES = (
    ('video cameras', 'video cameras'),
    ('photo cameras', 'photography cameras'),
    ('lighting', 'lighting'),
    ('lenses', 'lenses'),
    ('sound equipment', 'sound equipment'),
    ('grip', 'grip'),
    ('camera accessories', 'camera accessories'),
    ('drones', 'drones'),
    ('vehicles', 'vehicles'),
    ('props', 'props'),
)
class Category(models.Model):
    name = models.CharField(choices=CATEGORY_CHOICES, max_length=250)
    image = models.ImageField(upload_to='gallery')

    def __str__(self):
        return self.name
    
    def get_absolute_url(self):
        return reverse('equipment_list')


class Equipment(models.Model):
    category = models.CharField(choices=CATEGORY_CHOICES, max_length=250, default='uncategorized')
    name = models.CharField(max_length=250)
    slug = models.SlugField(max_length=250)
    image_1 = models.ImageField(upload_to='gallery', blank=True)
    image_2 = models.ImageField(upload_to='gallery', blank=True, null=True)
    image_3 = models.ImageField(upload_to='gallery', blank=True, null=True)
    image_4 = models.ImageField(upload_to='gallery', blank=True, null=True)
    description = models.TextField(default='about item')
    price = models.CharField(default=0, max_length=20)
    discount_price = models.FloatField(blank=True, null=True)
    #label = models.CharField(choices=LABEL_CHOICES, max_length=1, default='P')
    vendor_name = models.CharField(max_length=100, default='JPI Support, Next Thought Media')
    vendor_verified = models.CharField(default='Verified', max_length=15)
    vendor_number = models.CharField(max_length=13, default='07061962695')
    vendor_number_2 = models.CharField(max_length=13, default='07061962695', blank=True, null=True)
    vendor_location = models.CharField(max_length=200, default='Surulere, Lagos State, Nigeria')
    objects = models.Manager()

    def save(self, *args, **kwargs):
        value = self.name + '-' + self.vendor_name + '-' + self.category
        self.slug = slugify(value, allow_unicode=True)
        super().save(*args, **kwargs)

    def __str__(self):
        return self.name + ' | ' + str(self.category) + ' | ' + str(self.price)

    def get_absolute_url(self):
        return reverse('equipment_detail')
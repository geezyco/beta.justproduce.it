from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic.edit import CreateView
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import authenticate, login
from django.views.generic.edit import FormView
from django.contrib.auth.mixins import LoginRequiredMixin
from . import forms
from django.views.generic.list import ListView
from learn.models import Course
from django.views.generic.detail import DetailView

# For email
from django.core.mail import EmailMessage
from django.conf import settings
from django.template.loader import render_to_string

# Create your views here.

class StudentCourseDetailView(DetailView):
    model = Course
    template_name = 'students/course/detail.html'

    def get_queryset(self):
        qs = super().get_queryset()
        return qs.filter(students__in=[self.request.user])

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # get course object
        course = self.get_object()
        if 'module_id' in self.kwargs:
            # get current module
            context['module'] = course.modules.get(id=self.kwargs['module_id'])
        else:
            # get first module
            context['module'] = course.modules.all()[0]
        return context

class StudentCourseListView(LoginRequiredMixin, ListView):
    model = Course
    template_name = 'students/course/list.html'

    def get_queryset(self):
        qs = super().get_queryset()
        return qs.filter(students__in=[self.request.user])

class StudentRegistrationView(CreateView):
    template_name = 'students/student/registration.html'
    form_class = forms.CustomUserCreationForm
    success_url = reverse_lazy('home')

    def form_valid(self, form):
        result = super().form_valid(form)
        cd = form.cleaned_data

        from students.models import CustomUser
        import random
        n = random.randint(1000,9999)
        save_email = CustomUser.objects.get(email=cd['email'])
        save_email.username = cd['first_name'] + str(n)
        save_email.save()

        user = authenticate(email=cd['email'],
                            password=cd['password1'])
        login(self.request, user)
        print("*******************************")
        print(cd['email'])
        print(cd['password1'])
        

        template = render_to_string('registration_email.html', {'name': self.request.user.first_name})
        print(settings.EMAIL_HOST_USER, settings.EMAIL_HOST_PASSWORD)
        user_email = self.request.user.email
        print(user_email)
        print(self.request.user.first_name)
        email = EmailMessage(
            'Welcome to justproduceit(Open Beta)',
            template,
            settings.EMAIL_HOST_USER,
            [self.request.user.email]
            )
        email.fail_silently=False
        email.send()
        return result

class StudentEnrollCourseView(LoginRequiredMixin, FormView):
    course = None
    form_class = forms.CourseEnrollForm

    def form_valid(self, form):
        self.course = form.cleaned_data['course']
        self.course.students.add(self.request.user)
        return super().form_valid(form)

    def get_success_url(self):
        return reverse_lazy('student_course_detail', args=[self.course.id])    

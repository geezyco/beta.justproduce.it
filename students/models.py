from django.db import models
from django.contrib.auth.models import AbstractUser
from django.conf import settings

# Create your models here.

class CustomUser(AbstractUser):
    USERNAME_FIELD = 'email'
    email = models.EmailField('email address', unique=True) # changes email to unique and blank to false
    REQUIRED_FIELDS = ['username']

    # def set_username(self, sender, instance, **kwargs):
    #     if not instance.username:
    #         username = instance.first_name
    #         counter = 1
    #         while settings.AUTH_USER_MODEL.objects.filter(username=username):
    #             username = instance.first_name + str(counter)
    #             counter += 1
    #         instance.username = username
    # models.signals.pre_save.connect(set_username, sender=settings.AUTH_USER_MODEL)